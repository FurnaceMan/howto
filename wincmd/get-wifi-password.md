To find the WiFi password of a network connection on your computer.

Use this to list network profiles that exist.
```
netsh wlan show profile
```

Then use this to show the password:
```
netsh wlan show profile name="NAME-OF-NETWORK" key=clear
```

Where NAME-OF-NETWORK is the name of the wireless network you intend to use.

Look for the line containing "Key Content" under "Security Settings", or use
the following 'findstr' command.

```
netsh wlan show profile name="NAME-OF-NETWORK" key=clear |
    findstr /c:"Key Content"
```

You may need to run the command window 'as administrator' to do this.

