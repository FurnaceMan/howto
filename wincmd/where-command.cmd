@echo off

where /q bc2
if ERRORLEVEL 1 (
    echo Beyond Compare not found in the PATH
    goto :SOMETHINGWRONG
)

echo Success
exit /b 0

:SOMETHINGWRONG
rem Failure
exit/b 1
