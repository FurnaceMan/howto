# doskey macros

If you are tired of typing 'ls' in your Microsoft command window here's a
quick way to enjoy unix-like commands in a standard command window.

1. Create a list of macros in a a file.
For example, make a file 'macrolist.txt'
```
alias=doskey /macros
cat=type $*
cp=copy $*
less=more /e $*
ls=dir /a /d /o:n $*
mv=move $*
rm=del /q $*
touch=copy /b $1+,, $1
```

2. Run the doskey command to save the command macros.
```
doskey /macrofile=macrolist.txt
```

3. Enjoy some of your favorite unix (Linux) commands.

Keep in mind that these macros cannot be used in command scripts, they are
only available while using the command window interactively.

