@echo off

rem Copy the files listed in filelist.txt to INSTALL_ROOT\cmd

setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
cls
color 0e

cd /d "%~dp0"
echo.

if not defined INSTALL_ROOT (
    echo INSTALL_ROOT is not defined
    goto :SOMETHINGWRONG
)

pushd "%INSTALL_ROOT%\cmd"
if ERRORLEVEL 1 (
    echo %INSTALL_ROOT%\cmd does not exist
    goto :SOMETHINGWRONG
)
popd

for /f "tokens=* delims=" %%f in (filelist.txt) do (
    echo %%f
    copy /y "%%f" "%INSTALL_ROOT%\cmd
    if ERRORLEVEL 1 (
        echo unable to copy %%f
        goto :SOMETHINGWRONG
    )
)

:COMPLETION
color
exit/b 0


:SOMETHINGWRONG
color 0c
exit/b 1
