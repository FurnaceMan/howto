rem Add the required DLLs
pushd %INSTALL_ROOT%\bin
for /f "usebackq delims=" %%d in (`dir/b "%INSTALL_ROOT%\bin\*.exe"`) do (
        windeployqt --no-translations %%d
        )
popd
