@echo off

rem A cheatsheet of things that can be done using MS Windows batch commands

rem --- clear the screen
cls
rem --- pop down a line, looks nicer
echo.

rem --- Use setlocal to keep variables local.
rem --- Extensions and delayed expansion are optional but nice to have.
setlocal ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

rem --- move to the directory where this script is located
cd /d "%~dp0"

rem --- change the color of the terminal to indicate something is running
color 0e

rem --- grab a timestamp
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a st_secs=(%hr%*3600)+(%mn%*60)+%sc%


rem --- TODO do stuff here - this is your stuff
rem --- if success then goto :COMPLETION
rem --- if failure then goto :SOMETHINGWRONG
echo Sample BAT script

echo.
rem --- Give the user a choice how to proceed
choice /c YN /d Y /t 30 /m "Skip timeout and pause demo"
if %ERRORLEVEL% EQU 1 goto :SKIPTIMEOUT

rem --- Delay things a little bit
timeout /t 10

rem --- Wait indefinitely until the user is ready
pause

:SKIPTIMEOUT
echo.

rem --- make an example file tree
mkdir dir1 dir2
echo this is dir 1 > dir1\readme.txt
echo the quick red fox jumped over the lazy brown dog >> dir1\readme.txt
echo this is dir 2 > dir2\readme.txt
echo eget est lorem ipsum dolor sit amet consectetur >> dir2\readme.txt

rem --- loop through directories
for /f "usebackq tokens=*" %%i in (`dir/b/a:d`) do (
    pushd %%i

    rem With delayed expansion enabled, use exclamation to
    rem expand vars that change in a loop.
    echo !CD!

    rem this is how to read lines in a file
    for /f "tokens=*" %%l in (readme.txt) do (
        rem use the carat symbol in the echo command
        rem to escape special meaning
        echo ^<pre^>%%l^</pre^>
    )
    rem If the file you're reading is a simple list with no
    rem spaces in the words, this works as well.
    rem for /f %%l in (mylist.txt) do ...
    rem
    rem Or if you simply have a few things that can be
    rem hard-coded, this is even simpler
    rem for %%i in (one two three) do ...

    echo.
    popd
)

rmdir/s/q dir1
rmdir/s/q dir2

rem --- The command 'tasklist /fi "imagename eq dropbox.exe"' still returns an
rem --- errorlevel 0 on failure.  The findstr command will return the correct
rem --- errorlevel, so that you can determine if a necessary process is running.
tasklist /fi "username eq %USERNAME%" | findstr /r /c:"^Dropbox.exe" >nul
if ERRORLEVEL 1 (
    echo Dropbox does not seem to be running
    goto :SOMETHINGWRONG
)

rem --- When running a special app, check for it first and make sure
rem --- it complete successfully.
where /q qmake
if ERRORLEVEL 1 (
    echo qmake not found, set up the Qt development environment
    goto :SOMETHINGWRONG
)
qmake
if ERRORLEVEL 1 goto :SOMETHINGWRONG

rem --- randomly force an error
set /a "oddnum=%RANDOM% %% 2"
if %oddnum% EQU 1 mkdir dir3
rem --- an attempt to re-make a directory is an error
mkdir dir3
if ERRORLEVEL 1 goto :SOMETHINGWRONG
rmdir/s/q dir3


:COMPLETION
rem --- display how long the script took to run 
rem --- no, this doesn't work if the time crosses over midnight
set ti=%TIME%
set hr=%ti:~0,2%
set mn=%ti:~3,2%
if "%mn:~0,1%" == "0" set mn=%mn:~1,1%
set sc=%ti:~6,2%
if "%sc:~0,1%" == "0" set sc=%sc:~1,1%
set /a tt_secs=((%hr%*3600)+(%mn%*60)+%sc%)-%st_secs%
echo.
echo Completed in %tt_secs% seconds

rem --- return to the normal screen color
color

rem --- if run interactively, just exit.
rem --- if invoked from Windows explorer, pause so the display is viewable.
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 0
pause
rem --- if you use 'exit', when using this script within another script
rem --- make sure to 'call' this script instead of directly invoking it.
rem --- Use 'goto :EOF' if you don't want that, but then this script
rem --- won't set the ERRORLEVEL.
exit /b 0


:SOMETHINGWRONG
rem --- do cleanup and error reporting here
if exist dir3 rmdir/s/q dir3

rem --- set to a noticable color
color 0c

rem --- see above
echo %CMDCMDLINE% | find /i "/c" >nul
if ERRORLEVEL 1 exit /b 1
pause
exit /b 1

