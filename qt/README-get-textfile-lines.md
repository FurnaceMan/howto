```cpp
bool textLines(const QString &filename, QStringList &textlines)
{
   QFile infile(filename);
   if(infile.open(QIODevice::Text | QIODevice::ReadOnly))
   {
      QTextStream ts(&infile);
      while(!ts.atEnd())
         textlines.append(ts.readLine());
      return true;
   }
   else
   {
      return false;
   }
}
```