
// At one time, something was always crashing in Windows and filling up the
// report queue.  The only way to fix it was to clear the report queue because
// the admin was unable to figure out the problem.  Eventually, someone fixed
// it, but in the meantime, this function was used to fend off disaster.

bool pruneReportQueueDirectory()
{
   QString rqDirname =
      QString("%1/Microsoft/Windows/WER/ReportQueue").arg(QString(qgetenv("LOCALAPPDATA")));
   QDir reportQueueDir(rqDirname);
   
   if(!reportQueueDir.exists())
      return false;

   QStringList namelist = reportQueueDir.entryList(
      QstringList() << "AppCrash*" << "AppHang*",
      QDir::Dirs | QDir::NoDotAndDotDot);

   bool ok(true);
   
   QDate today(QDate::currentDate());
   for(const auto &dname : namelist)
   {
      QDir qdir(reportQueueDir);
      if(qdir.cd(dname))
      {
         QFileInfo fi(qdir, "memory.hdmp");
         if(fi.exists() &&
            (fi.lastModified().date().daysTo(today) > 5))
         {
            if(!qdir.removeRecursively())
            {
               qWarning() << "cannot delete" << dname;
               ok = false;
            }
         }
      }
      else
      {
         qWarning() << "cannot use" << qdir.absolutePath();
         ok = false;
      }
   }
   
   return ok;
}

