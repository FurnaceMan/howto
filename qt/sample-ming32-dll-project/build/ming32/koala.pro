TEMPLATE = lib

# shared is default, use 'static' for a static lib
CONFIG += c++14 build_all

# Other useful options
#
# CONFIG -= qt
# QT -= gui
# !CONFIG(console) {
# QT += widgets
# }

HDRTARGET=$$TARGET

build_pass:CONFIG(debug, debug|release) {
unix:TARGET = $$join(TARGET,,,_debug)
else:TARGET = $$join(TARGET,,,d)
}

CONFIG(shared, shared|static) {
win32:DEFINES += MAKE_KOALA_DLL
win32:LIBS += -luser32 -lws2_32
}

greaterThan(QT_MAJOR_VERSION, 4) {
QMAKE_CXXFLAGS += -fno-diagnostics-show-caret
DEFINES += QT_MESSAGELOGCONTEXT
}

INCLUDEPATH += . ../..

# header files to be installed
PUBHDRS = \
    ../../koala/koaladecl.h
    ../../koala/libversion.h

# add private headers
HEADERS = $$PUBHDRS \
    ../../koala/buildversion.h

SOURCES = \
    ../../koala/libversion.cpp

CONFIG(shared, shared|static) {
win32 {
    build_pass:CONFIG(release, debug|release) {
        instheaders.path = /include/%%{HDRTARGET}
        instheaders.files = $$PUBHDRS
    
        instlib.path =  /lib
        instlib.files = release/lib$${TARGET}.a
        instlib.CONFIG += no_check_exist
        target.path = /bin
        target.files = release/$${TARGET}.dll
        target.CONFIG += no_check_exist
        INSTALLS = target instlib instheaders
    }
 
    build_pass:CONFIG(debug, debug|release) {
        instlibd.path =  /lib
        instlibd.files = debug/lib$${TARGET}.a
        instlibd.CONFIG += no_check_exist
        target.path = /bin
        target.files = debug/$${TARGET}.dll
        target.CONFIG += no_check_exist
        INSTALLS = target instlibd
    }
}
}

CONFIG(static, shared|static) {
win32 {
    build_pass:CONFIG(release, debug|release) {
        instheaders.path = /include/%%{HDRTARGET}
        instheaders.files = $$PUBHDRS
        target.path = /lib
        target.files = release/lib$${TARGET}.a
        target.CONFIG += no_check_exist
        INSTALLS = target instheaders
    }

    build_pass:CONFIG(debug, debug|release) {
        target.path = /lib
        target.files = debug/lib$${TARGET}.a
        target.CONFIG += no_check_exist
        INSTALLS = target
    }
}
}
