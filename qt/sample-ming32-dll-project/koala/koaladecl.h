#ifndef H_KOALADECL
#define H_KOALADECL

#if (defined(MAKE_KOALA_DLL) && (defined(LINK_KOALA_DLL)))
#error "cannot define MAKE_KOALA_DLL and LINK_KOALA_DLL at the same time"
#endif

#if defined(MAKE_KOALA_DLL)
#if defined(_WIN32)
#define KOALA_DECL __declspec(dllexport)
#else
#define KOALA_DECL
#endif
#elif defined(LINK_KOALA_DLL)
#if defined(_WIN32)
#define KOALA_DECL __declspec(dllimport)
#else
#define KOALA_DECL
#endif
#else
#define KOALA_DECL
#endif

#endif

