#include <koala/libversion.h>
#include "buildversion.h"

namespace Koala
{
   QString libVersion()
   {
      return {buildVersion};
   }
}
