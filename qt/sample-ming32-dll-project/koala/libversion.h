#ifndef H_LIBVERSION
#define H_LIBVERSION

#include <koala/koaladecl.h>
#include <QString>

namespace Koala
{
   extern KOALA_DECL QString libVersion();
}

#endif

