A short C++ function for Qt5 that generates the cryptographic hash string for
a given file.

```c++
QString fileChecksum(const QString &filename,
      QCryptographicHash::Algorithm method)
{
   QFile infile(filename);
   if(infile.open(QIODevice::ReadOnly))
   {
      QCryptographicHash crypto(method);
      if(crypto.addData(&infile))
         return crypto.result().toHex();
   }
   return {};
}
```
