[TOC]

Jimmy's HowTo Collection
========================

This is a bit more than a HowTo page, it's a collection of

* HowTo guides
* Cheatsheets
* Links to other sites
* Code Templates
* ...and whatever interesting information I see fit to include here

Really, this is just a collection bin for stuff I've written and don't
feel like deleting into oblivion.

## Useful coding links

* [StackOverflow](https://stackoverflow.com/)
* [CPP Reference](https://en.cppreference.com/w/)
* [Rosetta Code](http://rosettacode.org/wiki/Rosetta_Code)
* [SOLID](https://en.wikipedia.org/wiki/SOLID) principles
    1. Single responibility
    2. Open-closed
    3. Liskov substitution
    4. Interface segregation
    5. Dependency inversion

## About me

I'm an old school coder that likes to write desktop application code in C++,
especially using the Qt library.  I realize that it is better to use the cloud
and web-based apps, but sometimes a simple application written to be used
locally on a laptop simply works.

