#! /bin/bash

# Shortest substring match, from either the beginning or end

filename="bash.string.txt"

echo ${filename#*.}
echo ${filename%.*}
