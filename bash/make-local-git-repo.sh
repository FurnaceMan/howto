#!/bin/bash

# A simple script for building a local git repository on your computer.
# The limitation here is that the working directory and the repository need to
# be on the same drive.
#
# If the project name directory doesn't exist, this script makes one with a
# spike directory (for sandbox code) and a readme.md file.
#
# If the project directory does exist, the new project directory is named using
# '-withgit' added to the directory name to prevent banging into the original
# code directory.
# ------------------------------

if [ "%1" == "" ]; then
    echo usage: $0 PROJECTNAME
    exit 1
fi

# Adjust this path as necessary
REPODIR=/p/conman
if [ ! -d ${REPODIR} ]; then
   echo Edit this script and set REPODIR properly
   exit 1
fi

PROJNAME=`basename ${1}`

if [ -e ${REPODIR}/${PROJNAME}.git ]; then
   echo The ${PROJNAME} project already exists in ${REPODIR}
   exit 1
fi

if [ -d ${PROJNAME}-withgit ]; then
    echo That project has already been initialized with this script
    echo See the ${PROJNAME}-withgit directory
    exit 1
fi

# If the given directory doesn't exist, make an empty project with
# a readme file and a spike directory.
if [ -d $1 ]; then
   FRESHDIR=0
   pushd $1
else
   FRESHDIR=1
   mkdir $1
   pushd $1
   echo spike/ >> .gitignore
   echo The $1 project >> readme.md
fi

if [ -e .git/config ]; then
    # already is a git repository, make sure origin is not remote
    git remote remove origin 2> /dev/null
else
    # this needs to be initialized by Git
    git init
    git add .
    git commit -m 'initial project setup'
fi

# Move the repository directory and set it up as a bare repository.
# TODO make this so that it works across drives
mv .git ${REPODIR}/${PROJNAME}.git
pushd ${REPODIR}/${PROJNAME}.git
git config --bool core.bare true
popd

# clear out the freshly made working directory
if [ $FRESHDIR == 1 ]; then
    cd ..
    rm -rf ${PROJNAME}
fi
popd

# Then clone it and set up the spike subdirectory if necessary.
git clone file://${REPODIR}/${PROJNAME}.git ${PROJNAME}-withgit
if [ -d ${1}/spike ]; then
   mv ${1}/spike ${PROJNAME}-withgit
else
   mkdir ${PROJNAME}-withgit/spike
fi

if [ $FRESHDIR == 1 ]; then
   mv ${PROJNAME}-withgit ${PROJNAME}
   cd ${PROJNAME}
else
   cd ${PROJNAME}-withgit
fi

# (optional) Create a develop branch that gets tracked in the repository.
# git checkout -b develop master
# git push -u origin develop

# Display the status and branch list.  This may need some work for
# a project with an existing repository, especially with a develop branch.
echo
echo ----------
git remote -v
echo ----------
git status
echo ----------
git branch -a
echo ----------

