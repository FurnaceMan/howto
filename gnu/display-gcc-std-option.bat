rem Windows script to display value of the of the __cplusplus macro based on
rem the '-std' option used.
rem
rem Note that '-std=gnu++17' (or 14 or 2a or ??) enables GNU extensions.

g++ -std=c++98 -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++03 -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++11 -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++0x -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++14 -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++1y -dM -E -x c++ NUL | findstr cplusplus
g++ -std=c++17 -dM -E -x c++ NUL | findstr cplusplus

rem g++ -std=c++2a -dM -E -x c++ NUL | findstr cplusplus
