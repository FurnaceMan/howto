# How to move a locally created Git repository into Bitbucket

## Credit

[StackOverflow](https://stackoverflow.com/questions/15139128/two-step-git-import-over-ssh)

## Steps

In the steps below, adjust parameters as necessary for your situation.

0. If your repository is not local, make it local via a mirror clone.
    ```
    git clone --mirror git://someurl/somerepo.git
    ```

1. Create an empty repository on Bitbucket.
    See the site on how to do this.

2. Add bitbucket as a remote repository to your local repository.  
    ```
    git remote add bitbucket git@bitbucket.org:username/somerepo.git
    ```

3. Push everything out to Bitbucket.
    ```
    git push -u bitbucket --all
    ```

4. To be sure things are clean, re-clone the repository.  Just make sure you
   aren't overwriting the original working directory.
    ```
    git clone git@bitbucket.org:Username/somerepo.git
    ```

