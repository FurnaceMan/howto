# Renaming 'master' branch to something else

Credit goes to [Git Tower][renamemaster]

## Steps

1. Rename 'master' in local repo and push out the 'main' branch
```
git branch -m master main
git push -u origin main
```
Of course, you can replace 'main' with a name of your choosing.


2. Go to Bitbucket or equivalent and set the default branch to 'main'.

    On Bitbucket, go to the repository, click 'repository settings' on the
    left side.  Then click on 'advance' near the bottom of the page.  Set the
    'Main branch' setting to 'main'.


3. Now you can delete the 'master' on the remote repository.
```
git push origin --delete master
```

4. Teammates need to do the following.
```
git checkout master
git branch -m master main
git fetch
git branch --unset-upstream
git branch -u origin/main
```

- - - - -

[renamemaster]: https://www.git-tower.com/learn/git/faq/git-rename-master-to-main/
