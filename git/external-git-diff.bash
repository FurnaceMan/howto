#!/bin/bash

# External Diff script
#
# Put this file somewhere (for example: ~/local/bin)
#
# Then add the following to ~/.gitconfig
#
# [diff]
#    external = ~/local/bin/external-git-diff
#
# ----------
# The arguments passed to this script by Git
#
# 1. path (with filename)
# 2. old-file
# 3. old-hex
# 4. old-mode
# 5. new-file
# 6. new-hex
# 7. new-mode


# Untracked files don't get passed to the diff utility in the first place.
# So this check is not entirely necessary, but just in case, it's here.
if [ "${2##*/}" == "null" ]; then
   echo $1 is a new file
   exit 0
fi

# Show that a file has been removed
if [ "${5##*/}" == "null" ]; then
   echo $1 has been deleted
   exit 0
fi

# Show what has changed
echo $5 has differences

# The buildversion files aren't that important, ymmv
if [ "${5##*/}" == "buildversion.bat" ]; then exit 0; fi
if [ "${5##*/}" == "buildversion.h" ]; then exit 0; fi
if [ "${5##*/}" == "buildversion.iss" ]; then exit 0; fi
if [ "${5##*/}" == "buildversion.pri" ]; then exit 0; fi
if [ "${5##*/}" == "buildversion.sh" ]; then exit 0; fi

# Throw the differences up in Beyond Compare,
# adjust this path and application as necessary
/c/Program\ Files\ \(x86\)/Beyond\ Compare\ 2/bc2.exe "$2" "$5"

# Some folks append '| cat' to the end of the command to return a consistent
# return status.  I don't see a need for it tho.

