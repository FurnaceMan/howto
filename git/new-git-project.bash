#!/bin/bash

# Script for building a local git repository on your computer.
#
# It can make a new working directory, or take an existing directory
# and set it up into a git repository.  The original directory is renamed
# to DIRNAME-nogit.

if [ "$1" == "" ]; then
   echo usage: $0 PROJECTNAME
   exit 1
fi

# TODO: Adjust this path as necessary
# REPODIR=/p/repo
if [ ! -e ${REPODIR} ]; then
   echo Edit this script and set REPODIR
   exit 1
fi

if [ -e ${REPODIR}/${1}.git ]; then
   echo That project already exists
   exit 1
fi

# If the given directory doesn't exist, make an empty project with
# a readme file and a spike directory.
if [ -d $1 ]; then
   cd $1
   FRESHDIR=0
else
   mkdir $1
   cd $1
   echo spike/ >> .gitignore
   echo The $1 project >> README.md
   FRESHDIR=1
fi

# Set up the git repository in the working directory, then move it to
# the repository directory and set it up as a bare repository.
# Then clone it and set up the spike subdirectory if necessary.
git init
git add .
git commit -m 'project setup'
mv .git ${REPODIR}/${1}.git
pushd ${REPODIR}/${1}.git
git config --bool core.bare true
popd
cd ..
if [ $FRESHDIR == 1 ]; then
   rm -rf ${1}
else
   mv ${1} ${1}-nogit
fi

if [ -d ${1} ]; then
   echo Make sure no process is using the ${1} directory,
   echo then run this script again.
   echo
   echo Deleting the newly created repository...
   rm -rf ${REPODIR}/${1}.git
   exit 1
fi

git clone file://${REPODIR}/${1}.git
if [ -d ${1}-nogit/spike ]; then
   mv ${1}-nogit/spike ${1}
else
   mkdir ${1}/spike
fi

# Create a develop branch that gets tracked in the repository.
# Display the status and branch list.
cd $1
git checkout -b develop master
git push -u origin develop
echo ----------
git remote -v
echo ----------
git status
echo ----------
git branch -a
echo ----------
