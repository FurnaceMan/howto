# Markdown howto readme

* June Castillote [article](https://adamtheautomator.com/convert-markdown-to-html/) on converting Markdown to HTML.  (_recommended reading_)
    1. Powershell is mentioned as a method, but it requires powershell 7.  My
       version is 5.1.  I don't really want to screw around with adjusting
       powershell, when Windows should already do that.
    2. Pandoc is another method.  The article states that [Chocolately][choco]
       is required for installation, but it's not really required.  Choco is
       worth investigating though.
    3. Another utiliy, named Showdown, requires [Node.js][nodejs].  Again,
       adding complexity.

* [Pandoc](https://pandoc.org/demos.html) utility for document conversion.
* [maddy](https://opensourcelibs.com/lib/maddy) C++ markdown to HTML
  header-only parser library.  Not sure what is meant by header-only.
* [Aspose](https://www.nuget.org/packages/Aspose.Words.Cpp/) API for word
  processing.

- - - - -

[choco]: https://chocolatey.org/  "Package manager for windows"
[notejs]: https://nodejs.org/en/  "Javascript runtime built on Chrome V8 Javascript engine"
