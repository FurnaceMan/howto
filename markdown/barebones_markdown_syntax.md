Level 1 heading
===============

# Also a level 1 heading

Level 2 heading
---------------

## Also a level 2 heading

### Levels 1-6 use corresponding number of hashtags at the start of the line.

A horizontal line (below)
- - -
This is an example of a paragraph of continuous text.  A horizontal line (above) can be created using 3 or more dashes with no spaces between, but then the text above would make it into a level 2 heading.  Also level 1 and 2 headings can use the corresponding number of hashtags, but they typically have lines below them when shown as HTML, so using a bar in the text makes it more consistent.

Separate paragraphs with a blank line or put two spaces at the end of a line to force a simple line break.  
Text with _emphasis_  
Text that is **strong**  
Use backticks to show `commands`  
This is how to [specify a link](http://www.w3schools.com)  
A [reference link][link1] can be specified anywhere in the document.

    This text is indented 4 spaces.  Different markdown syntaxes do
    different things with this.  This is a code block and continues
    until a line indented less than 4 spaces is encountered.
        This text is indented 8 spaces

This is a separate paragraph

> This text is block quoted with a vertical bar on the left.
> > This text is block quoted again.


* Unordered list item 1  
  `command`  
  `command`  
  `command`

* Unordered list item 2
  more text here
    * sub item 1
    * sub item 2

1. Ordered list item 1 (use period)
2. Ordered list item 2

### A Sample Table

No align  | Right    | Left   | Center
----------|---------:|:-------|:------:
No        | Computer | $1600  | one
Colons    | Phone    | $12    | three
Test row  | With     | $1     | five
          | multiple |        |
          | lines    |        |
Last row  | Data     | $999   | six

```
+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | first line    | first line         |
|               | next line     | next line          |
+---------------+---------------+--------------------+
| Bananas       | first line    | first line         |
|               | next line     | next line          |
+---------------+---------------+--------------------+
```
### Some sample C++ code

```cpp
#include <iostream>
void main()
{
   std::cout << "Hello world" << endl;
}
```

[link1]: http://www.google.com/  "Typically, the link def is put at the bottom"
