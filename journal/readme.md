[TOC]

# Jim's Journal

A log of what things I've encountered.

## Sep 30, 2023

**Stop and Think**

Just tried to log on to my laptop (Windows 11) and started getting static
about my profile having issues.  Eventually, I was able to log in and after a
long wait encountered a brand new desktop.  A quick check conformed that I was
not only given a new profile, but a temporary profile.  A temporary profile is
like checking into a hotel room.  Once you check out, the room is cleaned and
set for the next visitor.  When you log out of a temporary profile, everything
you did and saved on that profile gets wiped out.  To confirm I really had a
temporary profile I brought up a command window.

1. Hit Win-R
2. Type **cmd** and hit enter.

The prompt you would normally see is something like `c:\users\myname`, where
'myname' is your name or something like it.  This time, I saw
`c:\users\TEMP.COMPUTERNAME.000`.  Fortunately I had my phone nearby and did a
search on 'windows temporary profile'.  I will put the link and a summary of
what I did in the Microsoft area of this howto.

More importantly, the initial shock that I may have lost my files and how I go
about using my laptop made me stop and think.  If everything I had on my
laptop was lost, I would run into a lot of problems.  I have a dropbox account
and I keep a bit of code here on Bitbucket.  I also have a mixmash of hard
drives and memory sticks with unorganized junk on them.  But this made things
really hit home for me.  I need to back up what I use on my laptop to a memory
stick that's readily available.

