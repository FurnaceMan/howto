# PowerShell Help

Windows PowerShell does not include help files, but you can read the
help topics online, or use the Update-Help cmdlet to download help files
to your computer and then use the Get-Help cmdlet to display the help
topics at the command line.

You can also use the Update-Help cmdlet to download updated help files
as they are released so that your local help content is never obsolete.

Without help files, Get-Help displays auto-generated help for cmdlets,
functions, and scripts.

## How to update PowerShell
In an existing powershell window enter:
```
winget search Microsoft.PowerShell
```
You will see a table with the latest PS version, note the Id.

Install PowerShell (or preview) by doing:
```
winget install -=id Microsoft.Powershell --source winget
```

This installs a new powershell under _c:\Program Files_.  In the command
prompt (Windows 11), click the down-arrow menu and select 'PowerShell', not
'Windows PowerShell'.

## How to convert Markdown to HTML
```
$md = ConvertFrom-Markdown -Path .\sample_readme.md
$md.Html | Out-File -Encoding utf8 .\sample_readme.html
```

## How to Get system information
```
systeminfo | Select-String "^OS Name","^OS Version"
```

## How to Get powershell version
```
$psversiontable.psversion
```


## How to run a powershell script bypassing restrictions
```
Get-Content sample.ps1 | powershell.exe -noprofile -
```

## Powershell cheatsheet
```ps1
get-alias cd                           Show alias for cd
set-location c:\users                  Change working directory
get-childitem c:\                      Directory command
```

