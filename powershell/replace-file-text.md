Use this powershell command to replace text:
```
powershell -Command "(gc filename.ext).replace('NnNn', 'MyPractice') | Out-File -Encoding ASCII filename.ext"
```