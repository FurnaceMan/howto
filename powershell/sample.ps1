<#
   an example of
   a multi line comment
#>

get-alias cd                           # Show alias for cd
set-location c:\users                  # Change working directory
get-childitem c:\                      # Directory command
