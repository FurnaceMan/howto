## Disable Aero Shake

### Via Windows Registry
    1. Run regedit
    2. Navigate to:
       `Computer\HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced`
    3. Adjust or create the key:
      `DisallowShaking, DWORD(32-bit), value of 1`

An alternative setting can also be used:
    1. Run regedit
    2. Navigate to:
       `HKEY_CURRENT_USER\Software\Policies\Microsoft\Windows`
    3. Create/Navigate to the `Explorer` key.
    4. Adjust or create the key:
      `NoWindowMinimizingShortcuts, DWORD(32-bit), value of 1`
    5. restart computer

### Disable Aero Shake Windows 10 in Group Policy
1. Open Windows Local Group Policy Editor.
2. Go to User Configuration -> Administrative Templates -> Desktop.
3. Double click on Turn off Aero Shake window minimizing mouse gesture.
4. Check Enabled and click OK.

### Windows 10 Disable Shake to Minimize via Settings (Pro Version)
1. Open Settings.
2. Select System.
3. Shift to Multitasking.
4. Turn the corresponding option to Off.

