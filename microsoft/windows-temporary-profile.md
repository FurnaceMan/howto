
From [Windows Temporary User Profile Issue](https://answers.microsoft.com/en-us/windows/forum/all/windows-temporary-user-profile-temp-issue/5fbefc7c-0b82-4395-bfda-6d7e2ebeacc6)

1. make a backup of important stuff in your USERPROFILE area.
2. Command prompt
```
whoami /user > whoami.txt
notepad whoami.txt
```
3. In Regedit goto `HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList`
4. Look for the SID key in the whoami document.
5. I had both the SID key and a SID.bak entry.
    * Delete the SID key (non-bak version)
    * Rename the 'bak' key to remove the '.bak' portion.
    * Make sure *ProfileImagePath* is set to the correct userprofile directory
    * Make sure *State* is set to zero.
6. See the site for what to do if you only have a key with no '.bak' extension
   or only a key with a '.bak' extension.
7. Exit the Registry and reset the computer
8. Log back in.  If it's not fixed, create a new profile as explained in that
   site link.

