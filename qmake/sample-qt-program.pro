CONFIG += c++17

greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
}

HEADERS = koala.h \
          centralwidget.h

SOURCES = main.cpp \
          koala.cpp \
          centralwidget.cpp

FORMS = centralwidget.ui

# When INSTALL_ROOT is defined 'install' will put
# the product in INSTALL_ROOT/bin.
target.path = /bin
INSTALLS += target

