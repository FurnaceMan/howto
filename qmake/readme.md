# qmake readme

The qmake utility is used when programming with the Qt library.  It makes use
of a PRO file to generate a Makefile that can be used by the well known 'make'
utility to build a source code project.

Use `qmake -project` in a source directory to generate a PRO file for use with
Qt.  Or you can roll your own PRO file.

## Console (command-line) application

For a standard C++ 'helloworld' type program that doesn't use the Qt
library, the following PRO file is all that is needed.

```
CONFIG += console
CONFIG -= qt

SOURCES = main.cpp
```

Or, to get a little more fancy...
```
CONFIG += c++17 console
CONFIG -= qt

# Uses a source file with the same base
# name as this PRO file.
SOURCES = $$join(TARGET,,,.cpp)

# define INSTALL_ROOT to set where to install
target.path = /bin
INSTALLS += target
```
