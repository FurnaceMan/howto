// user can't seem to get this to work
// yes, it's from 1998, but I printed it, so it must mean something

#include <winsock2.h>
#include <ws2tcpip.h>

#define HELLO_PORT 12345
#define HELLO_GROUP "255.0.0.37"

int main(int argc, char* argv[])
{
    struct sockaddr_in addr;
    int fd, cnt;
    struct ip_mreq mreq;
    char *message="Hello, World!";

    WSADATA wsaData;
    WSAStartup(MAKEWORD(1,1), &wsaData);

    if((fd=socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket");
        exit(1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(HELLO_GROUP);
    addr.sin_port = htons(HELLO_PORT);

    int ttl = 128;

    if( setsockopt(fd, IPPROTO_IP, IP_MULTICAST_TTL, (char*)&ttl,
                sizeof(ttl)) < 0)
    {
        printf("setsockopt: %u\n", WSAGetLastError());
    }

    return EXIT_SUCCESS;
}
